import React, { useState } from 'react';
import './App.css';
import Person from './Person/Person';

//Before hooks 
//class App extends Component {
//After hooks
const app = props => {
  const [ personState, setPersonsState ] = useState({
    persons: [
      {name: "Luis", age: 64},
      {name: "Nathas", age: Math.floor(Math.random() *30)},
      {name: "Mike", age: Math.floor(Math.random() *30)}
    ],
    otherState: 'Other things'
  });
  
const switchNameHandler = () => {
  setPersonsState({
      persons: [
      {name: "Luis", age: Math.floor(Math.random() *30)},
      {name: "Nathas", age: Math.floor(Math.random() *30)},
      {name: "Mike", age: Math.floor(Math.random() *30)}
    ]
  });
};

    return (
      <div className="App">        
        <h1>Hi, I'm a React Developer</h1>
        <h2>And I'll be the best one</h2>
        <span>This is fine :) </span> <br /> 
        <button onClick={switchNameHandler}>Switch Name</button>
        <Person name={personState.persons[0].name} age={personState.persons[0].age}></Person>
        <Person name={personState.persons[1].name} age={personState.persons[1].age}></Person>
        <Person name={personState.persons[2].name} age={personState.persons[2].age}>I'm not a person, I'm a Kitty</Person>
      </div>
    );
}

export default app;

